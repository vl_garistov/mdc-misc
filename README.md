# MDC - Miscellaneous

Collection of small programs developed for Mechanical Design and Constructions Ltd. (MDC).

JSON_and_STEP_engraver combines the functionalities of the other three which are deprecated. It is intended to run on an Arduino Leonardo and to be used together with [MDC - Laser Stepper](https://gitlab.com/vl_garistov/mdc-laser_stepper). Schematics for the whole system are in "Schematic_MDC KUKA peripherals_2022-09-21.pdf".

Please see the instructions for [MDC - Laser Stepper](https://gitlab.com/vl_garistov/mdc-laser_stepper).
