/*
	mdc-miscellaneous
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==================================================================================

	File: Keyboard_2C29_interrupt.ino
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Opens the next file in EZCAD and fires the engraving laser.
	Uses the keyboard library to emulate keypresses, based on
	"Keyboard logout" example by Tom Igoe from March 2012.
	http://www.arduino.cc/en/Tutorial/KeyboardLogout
*/

#include <Keyboard.h>

#define PULLUP_PIN	12
//#define LED_PIN		13
#define TIMER_PIN	13

#define FILE_OPEN_DELAY   2500 // ms
#define MAX_PULSE_LENGHT  5000
#define MIN_PULSE_LENGHT  100
#define TICKS_PER_MS      250
#define ODD_TICKS_PER_OVF 36
#define MS_PER_OVERFLOW   262
// Modify this constant to change the number of files that are cycled
#define NUM_STEPS       3

typedef enum {FRONT_RISING, FRONT_FALLING} front_t;

typedef struct
{
	uint16_t falling_counter;
	uint16_t rising_counter;
	uint16_t falling_overflows;
	uint16_t rising_overflows;
	front_t front;
}
pulse_capture_t;

//void button_ISR(void);
void timer3_init(void);
void GPIO_init(void);
bool validate_pulse(void);
void change_file(void);

volatile bool triggered = false;
volatile pulse_capture_t pulse_capture =
{
	.falling_counter = 0,
	.rising_counter = 0,
	.falling_overflows = 0,
	.rising_overflows = 0,
	.front = FRONT_FALLING,
};

void setup()
{
	GPIO_init();
	// attachInterrupt(digitalPinToInterrupt(2), fire_the_laser, FALLING);
	Keyboard.begin();
	timer3_init();
}

void loop()
{
	if (triggered)
	{
		change_file();
		// Lower the flag
		triggered = false;
	}
}

void GPIO_init(void)
{
	pinMode(TIMER_PIN, INPUT);
	pinMode(PULLUP_PIN, INPUT_PULLUP);
	//pinMode(LED_PIN, OUTPUT);
	//digitalWrite(LED_PIN, HIGH);
}

void timer3_init(void)
{
	// Disable all interrupts
	cli();
	// Stop the timer
	TCCR3B = 0;
	// Clear the timer configuration
	TCCR3A = TCCR3C = TIMSK3 = TCNT3 = 0;
	// Configure input capture mode on falling edge (already configured when stopping the timer)
	// TCCR3B &= ~(1 << ICES3);
	// Enable the noise canceler
	TCCR3B |= (1 << ICNC3);
	// Enable input capture and timer overflow interrupts
	TIMSK3 |= (1 << ICIE3) | (1 << TOIE3);
	// Clear the interrupt flags
	TIFR3 = 0;
	// Start the timer on 250 kHz
	TCCR3B |= (1 << CS11) | (1 << CS10);
	// Enable all interrupts
	sei();
}

ISR(TIMER3_OVF_vect)
{
	pulse_capture.front == FRONT_FALLING ? pulse_capture.falling_overflows++ : pulse_capture.rising_overflows++;
}

ISR(TIMER3_CAPT_vect)
{
	if (pulse_capture.front == FRONT_FALLING)
	{
		pulse_capture.falling_counter = ICR3;

		// Configure input capture mode on rising edge
		TCCR3B |= (1 << ICES3);
		// Clear the interrupt flag
		TIFR3 |= (1 << ICF3);

		// Check if we missed a very short pulse
		if (digitalRead(TIMER_PIN))
		{
			// Configure input capture mode on falling edge
			TCCR3B &= ~(1 << ICES3);
			// Clear the interrupt flag
			TIFR3 |= (1 << ICF3);

			return;
		}

		pulse_capture.front = FRONT_RISING;
		pulse_capture.rising_overflows = pulse_capture.falling_overflows;
		
		//digitalWrite(LED_PIN, LOW);
	}
	else
	{
		pulse_capture.rising_counter = ICR3;
		
		// Configure input capture mode on falling edge
		TCCR3B &= ~(1 << ICES3);
		// Clear the interrupt flag
		TIFR3 |= (1 << ICF3);

		pulse_capture.front = FRONT_FALLING;
		
		if (validate_pulse())
		{
			triggered = true;
		}
		
		// Clear pulse_capture to prepare for a new pulse
		pulse_capture.rising_overflows = 0;
		pulse_capture.falling_overflows = 0;

		//digitalWrite(LED_PIN, HIGH);
	}
}

bool validate_pulse(void)
{
	uint16_t ticks;
	uint16_t overflows;
	uint32_t pulse_lenght;

	if (pulse_capture.rising_overflows >= pulse_capture.falling_overflows)
	{
		overflows = pulse_capture.rising_overflows - pulse_capture.falling_overflows;
	}
	else
	{
		return false;
	}
	if (overflows)
	{
		ticks = pulse_capture.rising_counter + (65535 - pulse_capture.falling_counter);
		// Detect integer overflow
		if (ticks < pulse_capture.rising_counter)
		{
			overflows++;
			if(!overflows)
			{
				return false;
			}
		}
	}
	else
	{
		ticks = pulse_capture.rising_counter - pulse_capture.falling_counter;
	}
	
	pulse_lenght = ticks / TICKS_PER_MS
							 + overflows * MS_PER_OVERFLOW
							 + (overflows * ODD_TICKS_PER_OVF) / TICKS_PER_MS;

	return pulse_lenght > MIN_PULSE_LENGHT && pulse_lenght < MAX_PULSE_LENGHT;
}

void change_file(void)
{
	static uint8_t next_step = 1;
	
	// Open the "Open file" menu
	Keyboard.write(KEY_LEFT_ALT);
	Keyboard.write('f');
	Keyboard.write('o');

	// Select the next file
	Keyboard.print("STEP-");
	Keyboard.print(next_step);
	Keyboard.print(".ezd");
	Keyboard.write(KEY_RETURN);
	
	delay(FILE_OPEN_DELAY);

	// Engrave
	//Keyboard.write(KEY_F2);

	// Increment the file counter
	next_step++;
	if (next_step > NUM_STEPS)
	{
		next_step = 1;
	}
}
