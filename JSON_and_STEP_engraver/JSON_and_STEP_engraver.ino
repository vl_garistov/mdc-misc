/*
	mdc-miscellaneous
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==================================================================================

	File: JSON_and_STEP_engraver.ino
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Depending on the mode switch either reads a JSON file from SD card and
	engraves multiple .ezd files specified number of times or opens the next
	STEP-x.ezd file.
*/

#include <Keyboard.h>
#include <SD.h>
#include <inttypes.h>
#include <ArduinoJson.h>

//#define DEBUG
//#define DRY_TEST
// Uncomment if the .ezd extensions are omitted from the JSON file
//#define OMITTED_EXTENSION

#define PULLUP_PIN			12
#define TIMER_PIN			13
#define SD_SS_PIN			4
#define LASER_START_PIN		2
#define UNUSED_BUTTON_PIN	A4
#define MODE_SWITCH_PIN		A5
// Don't change these without also changing encoder_read();
#define ENCODER_0_PIN		A3
#define ENCODER_1_PIN		A2
#define ENCODER_2_PIN		A1
#define ENCODER_3_PIN		A0

#define FILE_OPEN_DELAY		2500	// ms
#define FILE_SELECT_DELAY  200  // ms
#define RELAY_DEBOUNCE_TIME	100		// ms
#define MAX_PULSE_LENGHT	5000
#define MIN_PULSE_LENGHT	100
#define TICKS_PER_MS		250
#define ODD_TICKS_PER_OVF	36
#define MS_PER_OVERFLOW		262

#define ENGRAVINGS_LIST_KEY	"engravings"
#define REPETITIONS_KEY		"repetitions"
#define FILENAME_KEY		"filename"

typedef enum {FRONT_RISING, FRONT_FALLING} front_t;

typedef struct
{
	uint16_t falling_counter;
	uint16_t rising_counter;
	uint16_t falling_overflows;
	uint16_t rising_overflows;
	front_t front;
}
pulse_capture_t;

void timer3_init(void);
void GPIO_init(void);
bool validate_pulse(void);
void laser_open_file(const char *filename);
inline void engrave(void);
void encoder_init(void);
inline uint8_t encoder_read(void);
int execute_file(uint8_t file_number);
void wait_for_rotation(void);
void laser_start_ISR(void);
void change_step_file(void);

volatile bool json_triggered = false;
volatile bool step_triggered = false;
volatile bool laser_start = false;
volatile pulse_capture_t pulse_capture =
{
	.falling_counter = 0,
	.rising_counter = 0,
	.falling_overflows = 0,
	.rising_overflows = 0,
	.front = FRONT_FALLING,
};
// Enough for 30 different engravings
StaticJsonDocument<1024> parsed_json;

void setup()
{
	GPIO_init();
	Keyboard.begin();
	timer3_init();
	encoder_init();
	while (!SD.begin(SD_SS_PIN));

	#ifdef DEBUG
	Serial.begin(9600);
	while (!Serial);
	Serial.println("Waiting for button...");
	#endif
}

void loop()
{
	if (json_triggered)
	{
		//digitalWrite(LED_PIN, LOW);
		RXLED1;
		execute_file(encoder_read());
		//digitalWrite(LED_PIN, HIGH);
		RXLED0;

		#ifdef DEBUG
		Serial.println("Waiting for button...");
		#endif

		// Lower the flag
		json_triggered = false;
	}

	if (step_triggered)
	{
		change_step_file();

		// Lower the flag
		step_triggered = false;
	}
}

int execute_file(uint8_t file_number)
{
	char json_filename[9] = {'\0'};
	File json_file_handle;
	DeserializationError json_error;

	sprintf(json_filename, "/%d.jsn", file_number);
	json_file_handle = SD.open(json_filename, FILE_READ);

	if (json_file_handle)
	{
		json_error = deserializeJson(parsed_json, json_file_handle);

		if (!json_error)
		{
			JsonArray engravings_list = parsed_json[ENGRAVINGS_LIST_KEY].as<JsonArray>();
			// Iterate over the different files / engravings
			for (JsonArray::iterator engraving = engravings_list.begin(); engraving != engravings_list.end(); ++engraving)
			{
				uint8_t repetitions = engraving->as<JsonObject>()[REPETITIONS_KEY];
				char *ezd_filename = engraving->as<JsonObject>()[FILENAME_KEY];
				laser_start = false;
				wait_for_rotation();
				laser_open_file(ezd_filename);

				#ifdef DEBUG
				Serial.print("Engraving '");
				Serial.print(ezd_filename);
				Serial.print("' ");
				Serial.print(repetitions);
				Serial.println(" times.");
				#endif

				for (uint8_t i = 0; i < repetitions - 1; i++)
				{
					engrave();
					wait_for_rotation();
				}
				engrave();
			}
		}
		else
		{
			#ifdef DEBUG
			Serial.print("Error parsing file '");
			Serial.print(json_filename);
			Serial.println("'");
			#endif

			return -2;
		}
	}
	else
	{
		#ifdef DEBUG
		Serial.print("Error opening file '");
		Serial.print(json_filename);
		Serial.println("'");
		#endif

		return -1;
	}

	json_file_handle.close();

	return 0;
}

void encoder_init(void)
{
	pinMode(ENCODER_0_PIN, INPUT_PULLUP);
	pinMode(ENCODER_1_PIN, INPUT_PULLUP);
	pinMode(ENCODER_2_PIN, INPUT_PULLUP);
	pinMode(ENCODER_3_PIN, INPUT_PULLUP);
}

inline uint8_t encoder_read(void)
{
	return (~PINF & 0xF0) >> 4;
}

void GPIO_init(void)
{
	pinMode(TIMER_PIN, INPUT);
	pinMode(PULLUP_PIN, INPUT_PULLUP);
	pinMode(LASER_START_PIN, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(LASER_START_PIN), laser_start_ISR, FALLING);
	pinMode(UNUSED_BUTTON_PIN, INPUT_PULLUP);
	pinMode(MODE_SWITCH_PIN, INPUT_PULLUP);
	//pinMode(LED_PIN, OUTPUT);
	//digitalWrite(LED_PIN, HIGH);
}

void timer3_init(void)
{
	// Disable all interrupts
	cli();
	// Stop the timer
	TCCR3B = 0;
	// Clear the timer configuration
	TCCR3A = TCCR3C = TIMSK3 = TCNT3 = 0;
	// Configure input capture mode on falling edge (already configured when stopping the timer)
	// TCCR3B &= ~(1 << ICES3);
	// Enable the noise canceler
	TCCR3B |= (1 << ICNC3);
	// Enable input capture and timer overflow interrupts
	TIMSK3 |= (1 << ICIE3) | (1 << TOIE3);
	// Clear the interrupt flags
	TIFR3 = 0;
	// Start the timer on 250 kHz
	TCCR3B |= (1 << CS11) | (1 << CS10);
	// Enable all interrupts
	sei();
}

ISR(TIMER3_OVF_vect)
{
	pulse_capture.front == FRONT_FALLING ? pulse_capture.falling_overflows++ : pulse_capture.rising_overflows++;
}

ISR(TIMER3_CAPT_vect)
{
	if (pulse_capture.front == FRONT_FALLING)
	{
		pulse_capture.falling_counter = ICR3;

		// Configure input capture mode on rising edge
		TCCR3B |= (1 << ICES3);
		// Clear the interrupt flag
		TIFR3 |= (1 << ICF3);

		// Check if we missed a very short pulse
		if (digitalRead(TIMER_PIN))
		{
			// Configure input capture mode on falling edge
			TCCR3B &= ~(1 << ICES3);
			// Clear the interrupt flag
			TIFR3 |= (1 << ICF3);

			return;
		}

		pulse_capture.front = FRONT_RISING;
		pulse_capture.rising_overflows = pulse_capture.falling_overflows;
	}
	else
	{
		pulse_capture.rising_counter = ICR3;
		
		// Configure input capture mode on falling edge
		TCCR3B &= ~(1 << ICES3);
		// Clear the interrupt flag
		TIFR3 |= (1 << ICF3);

		pulse_capture.front = FRONT_FALLING;
		
		if (validate_pulse())
		{
			(digitalRead(MODE_SWITCH_PIN) == HIGH ? step_triggered : json_triggered ) = true;
		}
		
		// Clear pulse_capture to prepare for a new pulse
		pulse_capture.rising_overflows = 0;
		pulse_capture.falling_overflows = 0;
	}
}

bool validate_pulse(void)
{
	uint16_t ticks;
	uint16_t overflows;
	uint32_t pulse_lenght;

	if (pulse_capture.rising_overflows >= pulse_capture.falling_overflows)
	{
		overflows = pulse_capture.rising_overflows - pulse_capture.falling_overflows;
	}
	else
	{
		return false;
	}
	if (overflows)
	{
		ticks = pulse_capture.rising_counter + (65535 - pulse_capture.falling_counter);
		// Detect integer overflow
		if (ticks < pulse_capture.rising_counter)
		{
			overflows++;
			if(!overflows)
			{
				return false;
			}
		}
	}
	else
	{
		ticks = pulse_capture.rising_counter - pulse_capture.falling_counter;
	}
	
	pulse_lenght = ticks / TICKS_PER_MS
							 + overflows * MS_PER_OVERFLOW
							 + (overflows * ODD_TICKS_PER_OVF) / TICKS_PER_MS;

	return pulse_lenght > MIN_PULSE_LENGHT && pulse_lenght < MAX_PULSE_LENGHT;
}

void laser_open_file(const char *filename)
{
	#ifdef DEBUG
	Serial.print("Changing active file to '");
	Serial.print(filename);
	Serial.println("'");
	#endif

	#ifdef DRY_TEST
	return;
	#endif

	// Open the "Open file" menu
	Keyboard.write(KEY_LEFT_ALT);
	Keyboard.write('f');
	Keyboard.write('o');

	// Select the next file
	Keyboard.print(filename);
	#ifdef OMITTED_EXTENSION
	Keyboard.print(".ezd");
	#endif
	Keyboard.write(KEY_RETURN);
	
	delay(FILE_OPEN_DELAY);
}

inline void engrave(void)
{
	Keyboard.write(KEY_F2);

	#ifdef DEBUG
	Serial.println("IMMA FIRIN MAH LAZER!!!");
	#endif
}

// Waits until the stepper motor has rotated the table
void wait_for_rotation(void)
{
	#ifdef DRY_TEST
	return;
	#endif

	while (!laser_start);
	delay(RELAY_DEBOUNCE_TIME);
	laser_start = false;
}

void laser_start_ISR(void)
{
	laser_start = true;
}

void change_step_file(void)
{
	static uint8_t next_step = 1;

	if (next_step > encoder_read())
	{
		next_step = 1;
	}
	
	// Open the "Open file" menu
	Keyboard.write(KEY_LEFT_ALT);
	Keyboard.write('f');
	Keyboard.write('o');

  delay(FILE_SELECT_DELAY);

	// Select the next file
	Keyboard.print("STEP-");
	Keyboard.print(next_step);
	Keyboard.print(".ezd");
	Keyboard.write(KEY_RETURN);
	
	delay(FILE_OPEN_DELAY);

	// Engrave
	//Keyboard.write(KEY_F2);

	// Increment the file counter
	next_step++;
}
