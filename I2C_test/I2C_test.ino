/*
	mdc-miscellaneous
	Copyright (C) 2022  Mechanical Design and Constructions Ltd. <info@mdc-bg.com>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License along
	with this program; if not, write to the Free Software Foundation, Inc.,
	51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==================================================================================

	File: I2C_test.ino
	Author: Vladimir Garistov <vl.garistov@gmail.com>
	Brief: Helper program used to test the I2C communication
	between ESP32 and Arduino
*/

#include <Wire.h>
#include <WirePacker.h>

#define I2C_ADDRESS	0x33

typedef enum __attribute__((__packed__))
{
	NoEvent,
	EngravingBatchStarted,
	EngravingBatchFinished,
	EngravingBatchRemaining,
	EngravingBatchCancelled
}
mdc_prod_event_t;

// Add a union if logging for more subsystems is implemented
typedef struct __attribute__((__packed__))
{
	mdc_prod_event_t event;
	uint8_t remaining;
}
mdc_msg_t;

size_t i2c_write(uint8_t address, void *data, size_t size);
size_t i2c_read(uint8_t address, void *data, size_t size);

void setup()
{
	Wire.begin();
}

void loop()
{
	static mdc_msg_t msg = {.event = EngravingBatchRemaining, .remaining = 60};

	i2c_write(I2C_ADDRESS, &msg, sizeof(msg));
	msg.remaining--;
	delay(5000);
}

size_t i2c_write(uint8_t address, void *data, size_t size)
{
	WirePacker packer;
	int error_code;
	size_t bytes_written;

	bytes_written = packer.write((uint8_t *) data, size);
	packer.end();
	if (bytes_written != size)
	{
		return -1;
	}

	// Note that the packer adds a CRC and therefore the number of bytes written should be larger than size
	bytes_written = 0;
	Wire.beginTransmission(address);
	while (packer.available())
	{
		bytes_written += Wire.write(packer.read());
	}
	error_code =  Wire.endTransmission();

	return error_code ? 0: bytes_written;
}
